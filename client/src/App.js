import React from 'react';
import logo from './logo.jpg';
import {ApolloProvider} from 'react-apollo';
import ApolloClient from 'apollo-boost';
import './App.css';
import Launches from './components/Launches';
import {BrowserRouter as Router,Route} from 'react-router-dom';
import launch from './components/Launch';

const client=new ApolloClient({
  uri:'http://localhost:5000/graphql'
});
function App() {
  return (
    <ApolloProvider client={client}>
      <Router>
    <div className="container">
      <img 
      src={logo} 
      alt="logo" 
      style={{width:300,display:'block', margin:'auto'}}
       />
           {/* <Launches/> */}
          <Route exact="/" component={Launches} />
          <Route exact="/launch/:flight_number" component={Launch} />


    </div>
    </Router>
    </ApolloProvider>
  );
}

export default App;
