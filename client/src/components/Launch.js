import React, { Component,Fragment } from 'react'
import {Query} from 'react-apollo'
import {Link} from 'react-router-dom';
import classNames from 'classnames';
import { from, gql } from 'apollo-boost'

const LANCH_QUERY=gql`

query LaunchQuery($flight_number:Int!){
    lauch(flight_number:$flight_number){
      flight_number
      mission_name
      launch_date_local
      launch_success
      launch_year
      rocket{
          rocket_id
          rocket_name
          rocket_type
      }
    }
}


`;
export class Launch extends Component {
    render() {
        return (
            <Fragment>
                {
                    ({loading,error,data})=>{
                        if(loading) return <h4>Loading ....</h4>
                        if (error) {
                            console.log(error);                            
                        }
                        const {
                            flight_number,
                            mission_name,
                            launch_success,
                            launch_year,
                            rocket :{
                                rocket_id,
                                rocket_name,
                                rocket_type
                            } = data.launch;

                        
                        return (
                            <div>
                                <h1 className="display-4 my-4">
                                <span className="text-dark">Mission:</span>
                                {mission_name}
                            </h1>
                            <h4 className="mb-3">Launch Details</h4>
                            <ul className="list-group">
                                <li className="list-group-item">
                                   Launch Year:{launch_year}

                                </li>
                                <li className="list-group-item">
                                    Launch Successful :
                                    <span className={classNames({
                                        'text-susscc':launch_success,
                                        'text-danger': !launch_success
                                    })}>
                                     {flight_number ? 'Yes':'No'}
                                    </span>

                                </li>
                                
                            </ul>
                            <h4 className="my-3">Rocket Details</h4>
                            <ul className="list-group">
                                    <li className="list-group-item">Rocket ID:{rocket_id} </li>
                                    <li className="list-group-item">
                                        Rocket Name :{rocket_name}
                                    </li>

                                    <li className="list-group-item"> 
                                    rocket type:{rocket_type}
                                    </li>
                            
                            </ul>
                            <hr/>
                                     <Link to="/" className="btn btn-secondary">Back</Link>
                        </div>
                        )
                       
                    }
                }
            </Fragment>
        )
    }
}

export default Launch
