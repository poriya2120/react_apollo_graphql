import React from 'react';
import classNames from 'classnames';
import Moment from 'react-moment';
import { Link } from 'react-router-dom';
import classNames from 'classnames';



export default function LaunchItem({
  launch: { flight_number, mission_name, launch_date_local, launch_success }
}) {
  return (
    <div className="card card-body mb-3">
      <div className="row">
        <div className="col-md-9">
          <h4>
            {/* Mission:{' '} */}
            {/* <span
              className={classNames({
                'text-success': launch_success,
                'text-danger': !launch_success
              })}
            > */}
            
              Misssion:<span className={classNames({
                'text-success':launch_success,
                'text-danger':!launch_success

              })}>
                {mission_name}
                </span> 
            {/* </span> */}
          </h4>
          <p>
            Date:<Moment format="YYYY-MM-DD HH:mm">{launch_date_local} </Moment>
          </p>
        </div>
        <div className="col-md-3">
          <Link to={`/launch/${flight_number}`} className="btn btn-secondary"> Launch data</Link>
           
        </div>
      </div>
    </div>
  );
}
// import React from 'react'

// export default function LaunchItem( {launch:{flight_number,misson_name,launch_date_local,launch_success}} ) {
//     console.log(this.props.launch)
//     return (
//         <div className="card card-body mb-3">
//             <div className="row">
//                 <div className="col-md-9">
//                      <h4>Mission: {misson_name}</h4>
//                     <p>Date:{launch_date_local}</p>
//                 </div>
//                 <div className="col-md-3">
//                     <button className="btn btn-secondary">Launch data</button>
//                 </div>
//             </div>
           
//         </div>
//     )
// }
      {/* <Link to={`/launch/${flight_number}`} className="btn btn-secondary">
            Launch Details
          </Link> */}